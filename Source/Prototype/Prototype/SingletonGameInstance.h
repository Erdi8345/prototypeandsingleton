// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Prototype/Prototype/TaskManager.h"
#include "SingletonGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class PROTOTYPE_API USingletonGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	class UTaskManager* Instance;

public:
	UTaskManager* GetTaskManager();
};

inline UTaskManager* USingletonGameInstance::GetTaskManager()
{
	if (!Instance)
	{
		Instance = NewObject<UTaskManager>();
	}

	return Instance;
}